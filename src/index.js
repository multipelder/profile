import { render } from 'react-dom';

import App from './App.jsx';

import './index.scss';

render(App, window.app);
