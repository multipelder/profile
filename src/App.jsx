import React, { useState } from 'react';

const App = () => {
    const [counter, setCounter] = useState(0);
    return (
        <div
            onMouseEnter={() => setCounter(counter + 1)}
            onMouseLeave={() => setCounter(counter - 1)}
        >
            {counter}
        </div>
    );
};

export default <App />;
