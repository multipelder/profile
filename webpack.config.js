const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const autoprefixer = require('autoprefixer');

const DEV = process.env.NODE_ENV === 'development';

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        main: ['@babel/polyfill', './index.js'],
    },
    output: {
        filename: filename('js'),
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: plugins(),
    devtool: DEV ? 'inline-source-map' : '',
    devServer: {
        port: 4200,
        hot: DEV,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node-modules/,
                loader: {
                    loader: 'babel-loader',
                    options: babelOptions(),
                },
            },
            {
                test: /\.jsx$/,
                exclude: /node-modules/,
                loader: {
                    loader: 'babel-loader',
                    options: babelOptions(['@babel/preset-react']),
                },
            },
            { test: /\.scss$/, use: styleLoaders(['sass-loader']) },
            { test: /\.css$/, use: styleLoaders() },
        ],
    },
};

function filename (ext) {
    return DEV ? `[name].${ext}` : `[name].[hash].${ext}`;
}

function plugins () {
    const plugins = [
        new MiniCssExtractPlugin({ filename: filename('css') }),
        new HTMLWebpackPlugin({ template: './index.html' }),
        new CleanWebpackPlugin(),
    ];

    const productionPlugins = [new BundleAnalyzerPlugin()];
    const developmentPlugins = [];

    if (DEV) {
        plugins.push(...developmentPlugins);
    } else {
        plugins.push(...productionPlugins);
    }

    return plugins;
}

function babelOptions (presets = [], plugins = []) {
    const options = {
        presets: ['@babel/preset-env'],
        plugins: ['@babel/plugin-proposal-class-properties'],
    };

    if (presets.length) {
        options.presets.push(...presets);
    }

    if (plugins.length) {
        options.plugins.push(...plugins);
    }

    return options;
}

function styleLoaders (extraLoaders = []) {
    const loaders = [
        {
            loader: MiniCssExtractPlugin.loader,
            options: { hmr: DEV, reloadAll: true },
        },
        {
            loader: 'css-loader',
            options: {
                modules: {
                    auto: /\.module.(sc|sa|c)ss/,
                    exportGlobals: true,
                    localIdentName: '[name]_[local]_[hash:base64:6]',
                },
            }
        },
        {
            loader: 'postcss-loader',
            options: {
                plugins: [
                    autoprefixer({
                        grid: true,
                    }),
                ],
            },
        },
    ];

    if (extraLoaders.length) {
        loaders.push(...extraLoaders);
    }

    return loaders;
}
